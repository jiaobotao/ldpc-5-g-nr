#include <stdio.h>
#include <stdint.h>
#include <fstream>
#include <time.h>

#include "LDPC_Matrix.h"
#include "LDPC.h"

void PrintHelp()
{
	printf("Help:\n");
	printf("argv[1]    - 仿真结果保存文件\n");
	printf("-K         - (**必须**)设置LDPC码字包含的信息比特数\n");
	printf("-R         - (**必须**)设置LDPC码字的码率, 建议精确到小数点后6-8位\n");
	printf("-ITR       - (可选)设置译码最大迭代次数, 默认值20\n");
	printf("-E         - (可选)满足校验关系时提前退出迭代\n");
	printf("-min/-max/-inv  - (可选)设置信噪比$E_b/N_0$参数,分别为最小的$E_b/N_0$,最大的$E_b/N_0$和每次$E_b/N_0$递增,单位为dB,默认值分别为: -2 /20 /0.25\n");
	printf("-BER/-FER  - (可选)设置目标BER/FER,当译码性能超过设置的BER/FER时,直接退出\n");
	printf("-EF/-TF    - (可选)设置最小错误帧数/最大传输帧数,当仿真错误EF或者发送TF帧数时,退出仿真,默认值分别为20/1000000\n");
	printf("-S         - (可选)设置随机种子，默认值0\n");
	printf("-PrintInfo - (可选)打印详细信息\n");

	printf("-BP/-LBP   - (**必须选一个**)置信传播(Belief Propagation)算法/分层置信传播(Layered Belief Propagation)算法\n");
}

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		PrintHelp();
		getchar();
		return 0;
	}

	// Input Parameter Init
	char* OutputFileName = argv[1];
	uint64_t Seed = 0;
	uint64_t MinError = 20;
	uint64_t MaxTrans = 1e6;
	uint32_t K = 128;
	float Rate = 0.5;
	uint32_t MaxITR = 20;
	uint32_t isExitBeforeMaxItr = false;
	uint32_t isPrintInfo = false;
	float MinEbN0indB = -2;
	float MaxEbN0indB = 20;
	float InvEbN0indB = 0.25;
	char* DecodeMethod = nullptr;

	uint32_t TargetBerFer = 0;
	float TargetBER = 0;
	float TargetFER = 0;

	uint32_t Debug = 0;

	// 读取输入参数
	for (uint32_t i = 2; i < argc; i++)
	{
		// Seed
		if (strcmp(argv[i], "-S") == 0)
			Seed = _atoi64(argv[i + 1]);
		// MinError
		else if (strcmp(argv[i], "-EF") == 0)
			MinError = _atoi64(argv[i + 1]);
		// MaxTrans
		else if (strcmp(argv[i], "-TF") == 0)
			MaxTrans = _atoi64(argv[i + 1]);
		// K
		else if (strcmp(argv[i], "-K") == 0)
			K = _atoi64(argv[i + 1]);
		// Rate
		else if (strcmp(argv[i], "-R") == 0)
			Rate = atof(argv[i + 1]);
		// MaxITR
		else if (strcmp(argv[i], "-ITR") == 0)
			MaxITR = _atoi64(argv[i + 1]);
		// isExitBeforeMaxItr
		else if (strcmp(argv[i], "-E") == 0)
			isExitBeforeMaxItr = 1;
		// isPrintInfo
		else if (strcmp(argv[i], "-PrintInfo") == 0)
			isPrintInfo = 1;
		// MinEbN0indB
		else if (strcmp(argv[i], "-min") == 0)
			MinEbN0indB = atof(argv[i + 1]);
		// MaxEbN0indB
		else if (strcmp(argv[i], "-max") == 0)
			MaxEbN0indB = atof(argv[i + 1]);
		// InvEbN0indB
		else if (strcmp(argv[i], "-inv") == 0)
			InvEbN0indB = atof(argv[i + 1]);
		// DecodeMethod
		else if (strcmp(argv[i], "-BP") == 0)
			DecodeMethod = argv[i] + 1;
		else if (strcmp(argv[i], "-LBP") == 0)
			DecodeMethod = argv[i] + 1;

		// TargetBER
		else if (strcmp(argv[i], "-BER") == 0)
		{
			TargetBerFer = 1;
			TargetBER = atof(argv[i + 1]);
		}
		// TargetFER
		else if (strcmp(argv[i], "-FER") == 0)
		{
			TargetBerFer = 2;
			TargetFER = atof(argv[i + 1]);
		}

		// Debug
		else if (strcmp(argv[i], "-Debug") == 0)
			Debug = atof(argv[i + 1]);
	}

	LDPC_Matrix matrix;
	Load_LDPC_Matrix(matrix, K, Rate);
	Load_LDPC_Matrix_GPU(matrix);

	srand(Seed);
	uint64_t ErrorFrames = 0;
	uint64_t TransFrames = 0;
	uint64_t ErrorBits = 0;
	uint64_t TransBits = 0;
	float AveITR = 0;
	float AveTime = 0;
	std::ofstream fout(OutputFileName, std::ios::app);
	for (float ebn0 = MinEbN0indB; ebn0 <= MaxEbN0indB; ebn0 += InvEbN0indB)
	{
		time_t start = clock();

		if (DecodeMethod == nullptr)
			break;

		// Decode with Belief Propagation Algorithm
		else if (strcmp(DecodeMethod, "BP") == 0)
		{
			Simulation_BP(matrix, Seed, ebn0, MinError, MaxTrans, MaxITR, isExitBeforeMaxItr, isPrintInfo,
				ErrorBits, TransBits, ErrorFrames, TransFrames, AveITR, AveTime, Debug);
			printf("K=%d, R=%.2f, EbN0=%.2f, BER=%.2e, FER=%.2e, ITR=%.1f, Time=%.2fs, BP\n",
				K, matrix.Rate, ebn0, ErrorBits / (double)TransBits, ErrorFrames / (double)TransFrames,
				AveITR, (clock() - start) / (double)CLOCKS_PER_SEC);
		}


		// Decode with Layered Belief Propagation Algorithm
		else if (strcmp(DecodeMethod, "LBP") == 0)
		{
			Simulation_LayeredBP(matrix, Seed, ebn0, MinError, MaxTrans, MaxITR, isExitBeforeMaxItr, isPrintInfo,
				ErrorBits, TransBits, ErrorFrames, TransFrames, AveITR, AveTime, Debug);
			printf("K=%d, R=%.2f, EbN0=%.2f, BER=%.2e, FER=%.2e, ITR=%.1f, Time=%.2fs, LBP\n",
				K, matrix.Rate, ebn0, ErrorBits / (double)TransBits, ErrorFrames / (double)TransFrames,
				AveITR, (clock() - start) / (double)CLOCKS_PER_SEC);
		}

		else
		{
			printf("尚未实现该算法，请重新确认...\n");
			break;
		}

		// Log Result
		float ber = ErrorBits / (double)TransBits;
		float fer = ErrorFrames / (double)TransFrames;
		fout << K << "\t" << Rate << "\t";
		fout << matrix.Rate << "\t" << matrix.BG_Choosen << "\t" << matrix.a_idx << "\t" << matrix.Zc << "\t";
		fout << ebn0 << "\t";
		fout << ErrorBits << "\t" << TransBits << "\t" << ErrorFrames << "\t" << TransFrames << "\t";
		fout << ber << "\t" << fer << "\t";
		fout << AveITR << "\t" << AveTime << "\t" << (clock() - start) / (double)CLOCKS_PER_SEC << "\n";
		fout.flush();

		// Transmitted Max Frames
		if (TransFrames >= MaxTrans)
			break;

		// Target BER / FER
		if (TargetBerFer == 1)
		{
			if (ber <= TargetBER)
				break;
		}
		else if (TargetBerFer == 2)
		{
			if (fer <= TargetFER)
				break;
		}
	}
	fout.close();

	return 0;
}